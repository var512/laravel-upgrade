WIP of a legacy system that is now using Laravel 6 and PHP 7.4.

It's only a small portion of the entire system. Most of the domain/business logic and any code that could contain confidential or sensitive information was removed.

### Laravel 6
- Laravel Passport OAuth2
- SSR, DataTables API
- Versioned/stateless RESTful API
- Moving from *thephpleague/fractal* to Laravel resources

### MySQL 8.0
- InnoDB
- utf8mb4_0900_ai_ci
    - case insensitive and accent insensitive
    - max key length: 3072 bytes / 4 bytes = VARCHAR(768) 

### Etc
- Hosted on Heroku, OpenShift, DigitalOcean, Vultr
- yowsup (Python) for WhatsApp notifications
- PHP-CS-Fixer doesn't support 7.4 yet
