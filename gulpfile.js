const gulp = require('gulp');
const uglify = require('gulp-uglify');
const uglifycss = require('gulp-uglifycss');
const concat = require('gulp-concat');
const rev = require('gulp-rev');
const del = require('del');

const BUILD_BASE_PATH = 'public/assets';

/****************************************************
 * watch
****************************************************/
function watchCss() {
    gulp.watch('resources/assets/css/**/*.css', {'mode': 'poll'}, gulp.series(defaultCss));
}

function watchJs() {
    gulp.watch('resources/assets/js/**/*.js', {'mode': 'poll'}, gulp.series(defaultJs));
}

/****************************************************
 * limpar
****************************************************/
function limparCss() {
    return del(["./public/assets/css/*.css"]);
}

function limparJs() {
    return del(["./public/assets/js/*.js"]);
}

/****************************************************
 * default - aplicado em todas as páginas
****************************************************/
function appDefaultCss() {
    return gulp.src([
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'node_modules/select2/dist/css/select2.css',
        'node_modules/font-awesome/css/font-awesome.css',
        'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
        'node_modules/sweetalert/dist/sweetalert.css',
        'resources/assets/css/vendor/dataTables.bootstrap.css',
        'resources/assets/css/vendor/responsive.bootstrap.css',
        'resources/assets/css/shared.css',
    ])
        .pipe(uglifycss())
        .pipe(concat('default.css'))
        .pipe(rev())
        .pipe(gulp.dest(BUILD_BASE_PATH + '/css'));
}

function appDefaultJs() {
    return gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/sweetalert/dist/sweetalert.min.js',
        'node_modules/select2/dist/js/select2.js',
        'node_modules/select2/dist/js/i18n/pt-BR.js',
        'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
        'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js',
        'node_modules/highcharts-release/highcharts.js',
        'resources/assets/js/vendor/jquery.dataTables.js',
        'resources/assets/js/vendor/dataTables.bootstrap.js',
        'resources/assets/js/vendor/dataTables.responsive.js',
        'resources/assets/js/appform.js',
    ])
        .pipe(uglify())
        .pipe(concat('default.js'))
        .pipe(rev())
        .pipe(gulp.dest(BUILD_BASE_PATH + '/js'));
}

/****************************************************
 * standalone - app-file-upload
 ****************************************************/
function appFileUploadLimpar() {
    return del([
        'public/assets/css/standalone/file-upload-*.css',
        'public/assets/js/standalone/file-upload-*.js',
    ]);
}

function appFileUploadCss() {
    return gulp.src([
        'resources/assets/css/vendor/dropzone.css',
    ])
        .pipe(uglifycss())
        .pipe(concat('file-upload.css'))
        .pipe(rev())
        .pipe(gulp.dest(BUILD_BASE_PATH + '/css/standalone'));
}

function appFileUploadJs() {
    return gulp.src([
        'node_modules/dropzone/dist/min/dropzone.min.js',
    ])
        .pipe(uglify())
        .pipe(concat('file-upload.js'))
        .pipe(rev())
        .pipe(gulp.dest(BUILD_BASE_PATH + '/js/standalone'));
}

/****************************************************
 * admin
****************************************************/
function appAdminCss() {
    return gulp.src([
        'resources/assets/css/admin.css',
    ])
        .pipe(uglifycss())
        .pipe(concat('admin.css'))
        .pipe(rev())
        .pipe(gulp.dest(BUILD_BASE_PATH + '/css'));
}

/****************************************************
 * clientes
****************************************************/
function appClientesCss() {
    return gulp.src([
        'resources/assets/css/clientes.css',
    ])
        .pipe(uglifycss())
        .pipe(concat('clientes.css'))
        .pipe(rev())
        .pipe(gulp.dest(BUILD_BASE_PATH + '/css'));
}

/****************************************************
 * tarefas ocasionais / manuais
 ****************************************************/
function appCopyFonts() {
    return gulp
        .src(['node_modules/font-awesome/fonts/*'])
        .pipe(gulp.dest(BUILD_BASE_PATH + '/fonts'));
}

/****************************************************
 * tasks
****************************************************/
const defaultCss = gulp.series(limparCss, appDefaultCss, appAdminCss, appClientesCss);
const defaultJs = gulp.series(limparJs, appDefaultJs);
const watch = gulp.parallel(watchCss, watchJs);
const appFileUpload = gulp.series(appFileUploadLimpar, appFileUploadCss, appFileUploadJs);

/****************************************************
 * exports
****************************************************/
exports.css = defaultCss;
exports.js = defaultJs;
exports.watch = watch;
exports.fu = appFileUpload;
exports.fonts = appCopyFonts;
