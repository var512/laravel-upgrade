<?php

namespace App\Http\Requests\Admin\Relatorios;

use App\Http\Requests\Request;

class ArquivoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'texto' => 'nullable|max:25000',
            // ...
            // ...
            // ...
        ];

        return $rules;
    }
}
