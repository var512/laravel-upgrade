<?php

namespace App\Http\Requests\Admin\Relatorios;

use App\Http\Requests\Request;

class ArquivoStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $allowedExt = implode(',', [
            'xls',
        ]);

        $maxSizeKilobytes = 15360;

        $rules = [
            'file' => 'required|extensao:' . $allowedExt . '|max:' . $maxSizeKilobytes,
            // ...
            // ...
            // ...
        ];

        return $rules;
    }

    public function response(array $errors)
    {
        $responseStr = '';

        foreach ($errors as $field) {
            foreach ($field as $k => $v) {
                $responseStr .= $v . ' ';
            }
        }

        return response($responseStr, 422);
    }
}
