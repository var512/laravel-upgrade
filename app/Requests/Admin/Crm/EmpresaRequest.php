<?php

namespace App\Http\Requests\Admin\Crm;

use App\Http\Requests\Request;

class EmpresaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'razao_social' => 'required|max:200',
            'nome_fantasia' => 'nullable|max:200',
            'cnpj' => 'nullable|cnpj|cnpjUniqueEntidadeSession',
            // ...
            // ...
            // ...
        ];

        // atributos
        $atributos = !empty($this->request->get('atributos'))
            ? $this->request->get('atributos')
            : [];

        foreach ($atributos as $k => $v) {
            $rules['atributos.' . $k] = 'exists:crm_atributos,id|atributoTemMesmaEntidadeSession';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];

        // produtos
        $produtos = !empty($this->request->get('produtos'))
            ? $this->request->get('produtos')
            : [];

        foreach ($produtos as $k => $v) {
            $messages['produtos.' . $k . '.exists'] = 'O produto #' . ($v) . ' é inválido.';
        }

        // atributos
        $atributos = !empty($this->request->get('atributos'))
            ? $this->request->get('atributos')
            : [];

        foreach ($atributos as $k => $v) {
            $messages['atributos.' . $k . '.exists'] = 'O atributo #' . ($v) . ' é inválido.';
        }

        return $messages;
    }
}
