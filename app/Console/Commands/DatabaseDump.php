<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatabaseDump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:database-dump';

    /**
     * Backup do banco de dados
     *
     * @var string
     */
    protected $description = 'App: database dump';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment(PHP_EOL . 'Database dump started' . PHP_EOL);

        $filename = 'dump_' . date('Y_m_d_H_i_s');

        $command = '';
        $command .= 'mysqldump';
        $command .= ' -u' . env('DB_USERNAME');
        $command .= ' -p' . env('DB_PASSWORD');
        $command .= ' ' . env('DB_DATABASE');

        # default
        $command .= ' --opt';

        # produces a checkpoint that allows the dump to capture all data prior to the
        # checkpoint while receiving incoming changes. Those incoming changes do not
        # become part of the dump. That ensures the same point-in-time for all tables.
        $command .= ' --single-transaction';

        # dumps all stored procedures and stored functions
        $command .= ' --routines';

        # dumps all triggers for each table that has them
        $command .= ' --triggers';

        # gzip
        $command .= ' | gzip > ' . env('DB_BACKUP_FOLDER') . '/mysql/' . $filename . '.sql.gz';

        exec($command);
    }
}
