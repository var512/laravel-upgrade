<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Helpers\DateHelper;
use App\Helpers\UnitHelper;
use App\Models\Admin\Crm\Acao;
use Illuminate\Console\Command;

class NotificarWhatsapp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:notificar-whatsapp';

    /**
     * Notificações (envia hoje as ações do próximo dia e pendentes)
     *
     * @var string
     */
    protected $description = 'App: notificar WhatsApp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment(PHP_EOL . 'Notificação iniciada' . PHP_EOL);

        $marcador = UnitHelper::bytesToUnicode("\xF0\x9F\x92\xA2");
        $whatsappNumero = env('WHATSAPP_NOTIFICAR');

        if (empty($whatsappNumero)) {
            return $this->error('Notificação cancelada: env WHATSAPP_NOTIFICAR inválido');
        }

        $dataAlvo = DateHelper::carbonNowParaTimezoneAtiva();
        $dataAlvo = $dataAlvo->addDay(1);

        $acoesPendentes = Acao::with(['empresa'])
            ->whereNull('data_conclusao')
            ->whereDate('data_promessa', '<=', $dataAlvo->format('Y-m-d'))
            ->orderBy('data_promessa', 'asc')
            ->get();

        if ($acoesPendentes->isEmpty()) {
            return;
        }

        $mensagem = '';
        $mensagem .= 'Ações pendentes do dia [' . $dataAlvo->format('d/m/Y') . '] e em atraso' . PHP_EOL;

        foreach ($acoesPendentes as $k => $v) {
            $mensagem .= PHP_EOL;
            $mensagem .= PHP_EOL;
            $mensagem .= $marcador . ' ' . $v->data_promessa->format('d/m/Y - H:i') . PHP_EOL;
            $mensagem .= '- ' . $v->categoriaLabel . PHP_EOL;
            $mensagem .= '- Empresa: ' . $v->empresa->razao_social . PHP_EOL;

            if (!empty($v->contatos)) {
                foreach ($v->contatos as $contato) {
                    $mensagem .= '--- Contato: ' . $contato->nome . PHP_EOL;
                }
            }
        }

        $mensagem = escapeshellarg($mensagem);

        $command = '';
        $command .= '/var/www/whatsapp/bin/yowsup-cli app';
        $command .= ' --config /var/www/whatsapp/whatsapp.config';
        $command .= ' -s ' . $whatsappNumero;
        $command .= ' ' . $mensagem;

        exec($command);
    }
}
