<?php
declare(strict_types=1);

namespace App\Domain\Admin\Relatorios\Importador;

use App\Helpers\RelatoriosHelper;

abstract class AbstractImportador {
    /**
     * Executado antes da importação
     */
    abstract protected function before(): void;

    /**
     * Executado após o fim da importação
     */
    abstract protected function after(): void;

    /**
     * Procedimentos de importação que cada Importador deve implementar
     */
    abstract protected function procedimentoImportacao(): void;

    /**
     * Executa a importação
     */
    public final function importar(): void
    {
        RelatoriosHelper::removerTodosDados();

        $this->before();
        $this->procedimentoImportacao();
        $this->after();
    }
}
