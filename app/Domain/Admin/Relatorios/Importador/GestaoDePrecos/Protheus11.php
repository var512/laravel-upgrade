<?php
declare(strict_types=1);

namespace App\Domain\Admin\Relatorios\Importador\GestaoDePrecos;

use App\Domain\Admin\Relatorios\Importador\AbstractImportador;
use App\Helpers\RelatoriosHelper;
use App\Models\Admin\Relatorios\Arquivo;
use App\Models\Admin\Relatorios\GestaoDePrecos\Familia;
use App\Models\Admin\Relatorios\GestaoDePrecos\Item;
use App\Models\Admin\Relatorios\GestaoDePrecos\Produto;
use Illuminate\Support\Str;

class Protheus11 extends AbstractImportador {
    protected function before(): void
    {
        // ...
        // ...
        // ...
    }

    protected function after(): void
    {
        // ...
        // ...
        // ...
    }

    protected function procedimentoImportacao(): void
    {
        $arquivos = Arquivo::all();

        // ...
        // ...
        // ...
    }

    /**
     * Retorna o tipo de dado da linha atual de acordo com seus valores
     */
    protected function getTipoDado(array $valores): string
    {
        // ...
        // ...
        // ...
    }
}
