<?php
declare(strict_types=1);

namespace App\Domain\Admin\Relatorios\Visualizador\GestaoDePrecos;

use App\Domain\Admin\Relatorios\Visualizador\AbstractVisualizador;
use App\Models\Admin\Relatorios\GestaoDePrecos\Familia;
use App\Models\Admin\Relatorios\GestaoDePrecos\Item;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class GestaoDePrecos extends AbstractVisualizador {

    protected function before(): void
    {
        $this->setTitulo('Gestão de preços - Gerado em ' . $this->getNow()->format('d/m/Y H:i'));
        $this->setFilename('gestao-de-precos-' . $this->getNow()->format('d-m-Y-H-i-s'));

        view()->share([
            'formato' => $this->getFormato(),
            'titulo' => $this->getTitulo(),
            'dadosRelatorio' => $this->getDadosRelatorio(),
            'commonMarkConverter' => $this->getCommonMarkConverter(),
        ]);
    }

    protected function procedimentoVisualizacaoHtml(): View
    {
        return view('admin.relatorios.visualizador.GestaoDePrecos.padrao');
    }

    protected function procedimentoVisualizacaoPdf(): View
    {
        return view('admin.relatorios.visualizador.GestaoDePrecos.padrao')->render();
    }

    protected function getDadosRelatorio()
    {
        return Familia::with([
            'produtos' => function($query) {
                /*
                 * É necessário calcular os totais com os filtros aplicados aos itens desse produto
                 * para usar o ORDER BY e também listar apenas os produtos onde existem itens válidos
                 * após o filtro ser aplicado
                 *
                 * Sem ORDER BY (ou sem precisar esconder produtos sem itens) poderia ser simplificado para:
                 * $query->whereHas('itens', function($q) { $q->comFiltros(); });
                 */
                $query->select(DB::raw(
                    // ...
                    // ...
                    // ...
                ));

                $query->leftJoin('rlt_gdp_itens', 'rlt_gdp_itens.produto_id', '=', 'rlt_gdp_produtos.id');
                // ...
                // ...
                // ...
                $query->groupBy(
                    // ...
                    // ...
                    // ...
                );

                Item::aplicarFiltros($query->toBase());
            },
            'produtos.itens' => function($query) {
                $query
                    ->comFiltros()
                    ->orderBy('codigo_fornecedor', 'asc')
                    ->orderBy('data_emissao', 'asc');
            },
        ])->get();
    }
}
