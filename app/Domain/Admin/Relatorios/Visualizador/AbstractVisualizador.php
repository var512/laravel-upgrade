<?php
declare(strict_types=1);

namespace App\Domain\Admin\Relatorios\Visualizador;

use App\Helpers\RelatoriosHelper;
use App\Helpers\DateHelper;
use Carbon\Carbon;
use Knp\Snappy\Pdf;
use League\CommonMark\CommonMarkConverter;
use Illuminate\View\View;

abstract class AbstractVisualizador {
    private string $formato = '';
    private string $titulo = 'Relatório';
    private string $filename = 'relatorio';
    private Carbon $now;
    private CommonMarkConverter $commonMarkConverter;
    private Pdf $pdf;

    public function __construct()
    {
        // TODO inject deps construct (antes confirmar se formato ficou ok)
        $this->now = DateHelper::carbonNowParaTimezoneAtiva();
        $this->commonMarkConverter = new CommonMarkConverter;
    }

    /**
     * Retorna o formato do visualizador
     */
    public function getFormato(): string
    {
        return $this->formato;
    }

    /**
     * Define o formato
     */
    public function setFormato(string $formato)
    {
        $this->formato = $formato;
    }

    /**
     * Retorna o título
     */
    public function getTitulo(): string
    {
        return $this->titulo;
    }

    /**
     * Define o título
     */
    public function setTitulo(string $titulo): void
    {
        $this->titulo = $titulo;
    }

    /**
     * Retorna o filename
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * Define o filename
     */
    public function setFilename(string $filename): void
    {
        $this->filename = $filename;
    }

    /**
     * Retorna uma instância Carbon com o horário atual
     */
    public function getNow(): Carbon
    {
        return $this->now;
    }

    /**
     * Retorna a instância do CommonMarkConverter
     */
    public function getCommonMarkConverter(): CommonMarkConverter
    {
        return $this->commonMarkConverter;
    }

    /**
     * Executado antes da visualização
     */
    abstract protected function before(): void;

    /**
     * Inicializa a criação do PDF
     */
    protected function initPdf(): void
    {
        $pdf = new Pdf();
        $pdf->setBinary('LC_ALL=pt_BR.UTF-8 ' . base_path(env('WKHTMLTOPDF_BIN')));
        $pdf->setTemporaryFolder(ini_get('upload_tmp_dir'));

        $pdf->setOption('encoding', 'utf-8');
        $pdf->setOption('no-background', false);
        $pdf->setOption('lowquality', false);
        $pdf->setOption('orientation', 'Landscape'); // Portrait - Landscape
        $pdf->setOption('page-size', 'A4');
        $pdf->setOption('title', $this->getTitulo());
        $pdf->setOption('footer-font-size', 9);
        $pdf->setOption('footer-left', $this->getTitulo());
        $pdf->setOption('footer-right', '---');

        $this->pdf = $pdf;
    }

    /**
     * Retorna a instância do Pdf
     *
     * @return Pdf
     */
    public function getPdf(): Pdf
    {
        return $this->pdf;
    }

    /**
     * Procedimento de visualização HTML
     */
    abstract protected function procedimentoVisualizacaoHtml(): View;

    /**
     * Procedimento de visualização PDF
     */
    abstract protected function procedimentoVisualizacaoPdf(): View;

    /**
     * Executa a visualização
     *
     * @return mixed
     */
    public final function visualizar()
    {
        $this->before();

        $formato = $this->getFormato();

        if (empty($formato) || !array_key_exists($formato, RelatoriosHelper::listaFormatoVisualizador())) {
            app()->abort(500, 'Formato de relatório inválido');
        }

        switch ($formato) {
            case 'pdf':
                $this->initPdf();
                $output = $this->procedimentoVisualizacaoPdf();

                return response()->make($this->getPdf()->getOutputFromHtml($output), 200, [
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'inline;filename=' . $this->getFilename() . '.pdf',
                ]);

            case 'html':
                $output = $this->procedimentoVisualizacaoHtml();

                return $output;
        }
    }
}
