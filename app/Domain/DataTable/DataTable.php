<?php

declare(strict_types=1);

namespace App\Domain\DataTable;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

// TODO refactor, criar sistema para filtros sem usar traits, VOs
class DataTable
{
    protected Request $request;
    protected array $filtros;
    protected array $columns;
    protected array $order;
    protected int $start;
    protected int $resultsPerPage;

    public function __construct()
    {
        $this->request = request();
        $this->filtros = (array) $this->request->input('filtros');
        $this->columns = (array) $this->request->input('columns');
        $this->order = (array) $this->request->input('order');
        $this->start = (int) $this->request->input('start');
        $this->resultsPerPage = (int) $this->request->input('resultsPerPage');

        $this->setPaginatorResolver();
    }

    public function getFiltros(): array
    {
        return $this->filtros;
    }

    public function getOrderBy(): array
    {
        if (empty($this->order[0])) {
            return [];
        }

        $column = !empty($this->columns[$this->order[0]['column']]['name'])
            ? $this->columns[$this->order[0]['column']]['name']
            : null;

        $dir = !empty($this->order[0]['dir'])
            ? mb_strtolower($this->order[0]['dir'])
            : null;

        if (!in_array($dir, ['asc', 'desc'])) {
            $dir = null;
        }

        if (!empty($column) && !empty($dir)) {
            return [
                'column' => $column,
                'dir' => $dir,
            ];
        }

        return [];
    }

    protected function setPaginatorResolver(): void
    {
        $currentPage = 1;

        if (!empty($this->start)) {
            $currentPage = (int) ceil($this->start / $this->resultsPerPage + 1);
        }

        Paginator::currentPageResolver(function() use ($currentPage) {
            return $currentPage;
        });
    }
}
