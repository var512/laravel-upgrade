<?php

declare(strict_types=1);

namespace App\Models\Traits;

use App\Helpers\DateHelper;

trait TimezoneAwareModelTrait
{
    /**
     * Converte uma string de timestamp (UTC por padrão) para a timezone ativa
     */
    public function converterParaTimezone($value)
    {
        if (empty($value)) {
            return null;
        }

        return $this->asDateTime($value)
            ->copy()
            ->tz(DateHelper::getTimezoneAtiva());
    }

    /**
     * Converte o atributo created_at para a timezone ativa
     */
    public function getCreatedAtNaTimezoneAtivaAttribute($value)
    {
        return $this->converterParaTimezone($this->created_at);
    }

    /**
     * Converte o atributo updated_at para a timezone ativa
     */
    public function getUpdatedAtNaTimezoneAtivaAttribute($value)
    {
        return $this->converterParaTimezone($this->updated_at);
    }
}
