<?php

declare(strict_types=1);

namespace App\Helpers;

use Exception;
use Carbon\Carbon;

class DateHelper
{
    // TODO
    // - refatorar DateTime legacy de string para Carbon e remover os helpers daqui
    // - rever imutabilidade Carbon
    // - value objects

    /**
     * Retorna os valores do <select> de horario (ex: "t_01_05" => "01:05")
     */
    public static function listaHorario(): array
    {
        $horarios = [];

        $horas = range(0, 23);

        foreach ($horas as $k => $v) {
            for ($minutos = 0; $minutos <= 55; $minutos += 5) {
                $horaLabel = '';
                $horaLabel .= str_pad(strval($v), 2, '0', STR_PAD_LEFT);
                $horaLabel .= ':';
                $horaLabel .= str_pad(strval($minutos), 2, '0', STR_PAD_LEFT);

                $horaKey = 't_' . str_replace(':', '_', $horaLabel);

                $horarios[$horaKey] = $horaLabel;
            }
        }

        return $horarios;
    }

    /**
     * DateTime aleatória (usado @ factory)
     */
    public static function dateTimeAleatorio(int $diasAntes, int $diasDepois): string
    {
        $listaHorario =  array_values(self::listaHorario());
        $data = mt_rand(
            strtotime('-' . mt_rand(0, $diasAntes) . ' days'),
            strtotime('+' . mt_rand(0, $diasDepois) . ' days')
        );

        return date('Y-m-d', $data) . ' ' . $listaHorario[array_rand($listaHorario, 1)] . ':00';
    }

    /**
     * Timezone ativa
     */
    public static function getTimezoneAtiva(): string
    {
        return env('TIMEZONE_PADRAO');
    }

    /**
     * Retorna Carbon com a timezone ativa
     */
    public static function carbonNowParaTimezoneAtiva(): Carbon
    {
        return Carbon::now(static::getTimezoneAtiva());
    }

    /**
     * Recebe a horaKey (ex: t_23_55) e, se válido, retorna no formato de horário (ex: 23:55)
     */
    public static function keyToHora(string $horaKey): ?string
    {
        $listaHorario = self::listaHorario();

        if (!array_key_exists($horaKey, $listaHorario)) {
            return null;
        }

        return $listaHorario[$horaKey];
    }

    /**
     * Recebe o horário (ex: 23:55) e, se válido, retorna no formato horaKey (ex: t23_55)
     * @return mixed|null
     */
    public static function horaToKey(string $hora)
    {
        $listaHorario = self::listaHorario();

        if (!in_array($hora, $listaHorario)) {
            return null;
        }

        return array_search($hora, $listaHorario);
    }

    /**
     * Recebe a data (d/m/Y) e horaKey (ex: t_23_55) e retorna no formato DATETIME do MySQL.
     * Apenas formata e valida, sem aplicar modificações de timezone.
     */
    public static function dataHoraKeyParaMySqlDateTime(string $data, string $horaKey): ?string
    {
        $hora = self::keyToHora($horaKey);

        $datahora = $data . ' ' . $hora . ':00';

        try {
            $dateTime = Carbon::createFromFormat('d/m/Y H:i:s', $datahora)->format('Y-m-d H:i:s');
        } catch (Exception $e) {
            $dateTime = null;
        }

        return $dateTime;
    }
}
