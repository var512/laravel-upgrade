<?php

declare(strict_types=1);

namespace App\Helpers;

use App\Domain\Admin\Relatorios\Importador\GestaoDePrecos\Protheus11;
use App\Domain\Admin\Relatorios\Visualizador\GestaoDePrecos\GestaoDePrecos;
use App\Domain\Admin\Relatorios\Visualizador\InflacaoInterna\InflacaoInterna;
use App\Models\Admin\Relatorios\Arquivo;
use App\Models\Admin\Relatorios\GestaoDePrecos\Familia as GdpFamilia;
use App\Models\Admin\Relatorios\GestaoDePrecos\Item as GdpItem;
use App\Models\Admin\Relatorios\GestaoDePrecos\Produto as GdpProduto;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RelatoriosHelper
{
    /**
     * Retorna o prefixo / root do storage de acordo com o disk
     */
    private static function getStoragePathPrefix(): string
    {
        return Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
    }

    /**
     * Retorna a pasta onde o upload foi realizado
     */
    private static function getUploadFolder(): string
    {
        return env('ARQUIVOS_RELATORIOS_PATH');
    }

    /**
     * Path do arquivo salvo no storage
     */
    private static function getStorageFilepath(Arquivo $arquivo): string
    {
        $path = '';

        $path .= self::getUploadFolder();
        $path .= $arquivo->id;
        $path .= '.' . $arquivo->extensao;

        return $path;
    }

    /**
     * Testa se arquivo existe no disco
     */
    public static function arquivoExiste(Arquivo $arquivo): bool
    {
        return Storage::disk('local')->has(self::getStorageFilepath($arquivo));
    }

    /**
     * Retorna o caminho completo para o arquivo no disco
     */
    public static function getFullFilepath(Arquivo $arquivo): string
    {
        return self::getStoragePathPrefix() . self::getStorageFilepath($arquivo);
    }

    /**
     * Tipos de dados (de relatórios) que podem ser importados
     */
    public static function getTiposDeImportador(): Collection
    {
        // ...
        // ...
        // ...
    }

    /**
     * Retorna um tipo de importador com o id informado
     *
     * @param string $id Valor vindo do ID do relátorio (ex: "gdp_protheus11")
     */
    public static function getTipoDeImportadorById(string $id)
    {
        // ...
        // ...
        // ...
    }

    /**
     * Modelos (de relatórios) que podem ser visualizados
     *
     * @return Collection
     */
    public static function getTiposDeVisualizador(): Collection
    {
        // ...
        // ...
        // ...
    }

    /**
     * Retorna um modelo de visualizador com o id informado
     *
     * @param string $id Valor vindo do ID do visualizador (ex: "gdp")
     * @return mixed
     */
    public static function getModeloDeVisualizadorById(string $id) //:class@anonymous
    {
        return self::getTiposDeVisualizador()->where('id', $id)->first();
    }

    /**
     * Retorna os modelos de visualizadores
     * no formato padrão de listas
     */
    public static function listaModeloVisualizador(): Collection
    {
        // ...
        // ...
        // ...
    }

    /**
     * Lista de formatos do visualizador
     */
    public static function listaFormatoVisualizador(): array
    {
        return [
            'html' => 'HTML',
            'pdf' => 'PDF',
        ];
    }

    /**
     * Recebe o valor como float raw do Excel (sem envolver timezones) e converte para data
     */
    public static function excelFloatSemTzParaDatetime(float $valor): Carbon
    {
        return Carbon::createFromTimestamp($valor);
    }

    /**
     * Recebe o valor como Geral do Excel (sem envolver timezones) e converte para data
     * O formato é inválido (ex: 24.01.2016)
     */
    public static function excelDotsSemTzParaDatetime(string $valor): Carbon
    {
        return Carbon::createFromFormat('d.m.Y', $valor);
    }

    /**
     * Recebe o valor como Geral do Excel (sem envolver timezones) e converte para data
     * O formato é inválido (ex: 24/01/2016)
     */
    public static function excelSemTzParaDatetime(string $valor): Carbon
    {
        return Carbon::createFromFormat('d/m/Y', $valor);
    }

    /**
     * Prepara/limpa o valor (antes de o importar para o banco de dados)
     */
    public static function prepararValor($valor): string
    {
        // ...
        // ...
        // ...
    }

    /**
     * Ano usado para gerar a tabela de Inflação Interna
     */
    public static function getAnoInflacaoInterna(): int
    {
        return (int) date('Y');
    }
}
