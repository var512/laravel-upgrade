<?php

declare(strict_types=1);

namespace App\Helpers;

class ArrayHelper
{
    /**
     * Retorna um elemento da array ou um valor em branco
     * mantendo a mesma estrutura antes do merge
     */
    public static function valorAleatorioOuEmBranco(array $array): string
    {
        return array_rand(array_merge($array, ['' => '']));
    }
}
