<?php

declare(strict_types=1);

namespace App\Helpers;

class UnitHelper
{
    /**
     * Converte bytes para o formato humano
     * github.com/cknow/laravel-support
     */
    public static function formatarBytes(int $bytes): string
    {
        if ($bytes <= 0) {
            return '0 bytes';
        }

        $units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        $level = floor(log($bytes) / log(1024));
        $output = sprintf('%.2f %s', ($bytes / pow(1024, $level)), $units[intval($level)]);

        return $output;
    }

    /**
     * Recebe bytes UTF-8 (ex: "\xF0\x9F\x92\xA2") e retorna em unicode
     *
     * @param $unicodeChar
     * @return mixed
     */
    public static function bytesToUnicode(string $unicodeChar)
    {
        return json_decode('"' . $unicodeChar . '"');
    }
}
