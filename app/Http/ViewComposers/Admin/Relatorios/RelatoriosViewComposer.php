<?php

declare(strict_types=1);

namespace App\Http\ViewComposers\Admin\Relatorios;

use App\Helpers\RelatoriosHelper;
use Illuminate\Contracts\View\View;

class RelatoriosViewComposer {

    public function compose(View $view)
    {
        $data = [];

        // ...
        // ...
        // ...

        $view->with($data);
    }
}
