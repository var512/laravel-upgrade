<?php

declare(strict_types=1);

namespace App\Http\ViewComposers\Admin;

use App\Models\Admin\Crm\Entidade;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AdminViewComposer {

    public function compose(View $view)
    {
        $data = [];

        // ...
        // ...
        // ...

        $view->with($data);
    }

    protected function getAdministradoresData()
    {
        $environment = app()->environment();

        $navMenu = [];

        // ...
        // ...
        // ...

        return [
            'navMenu' => $navMenu,
        ];
    }

    protected function getClientesData()
    {
        $navMenu = [];

        // ...
        // ...
        // ...

        return [
            'navMenu' => $navMenu,
        ];
    }
}
