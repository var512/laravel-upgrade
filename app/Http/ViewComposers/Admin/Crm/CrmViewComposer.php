<?php

declare(strict_types=1);

namespace App\Http\ViewComposers\Admin\Crm;

use App\Models\Admin\Crm\Acao;
use App\Models\Admin\Crm\Anotacao;
use App\Models\Admin\Crm\Empresa;
use App\Models\DadosGenericos;
use Illuminate\Contracts\View\View;

class CrmViewComposer {

    public function compose(View $view)
    {
        $data = [];

        // ...
        // ...
        // ...

        $view->with($data);
    }
}
