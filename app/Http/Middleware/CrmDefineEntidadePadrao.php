<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\Admin\Crm\Entidade;
use Closure;
use Exception;

class CrmDefineEntidadePadrao
{
    /**
     * Define uma entidade padrão para novas sessions
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('admin.crm.entidade.id')) {
            try {
                $entidade = Entidade::findOrFail(1);
            } catch (Exception $e) {
                app()->abort(500, 'Erro ao definir entidade padrão');
            }

            $request->session()->put('admin.crm.entidade.id', $entidade->id);
        }

        return $next($request);
    }
}
