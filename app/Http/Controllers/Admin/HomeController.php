<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use CacheTool\Adapter\FastCGI;
use CacheTool\CacheTool;
use CacheTool\Proxy\OpcacheProxy;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{
    // TODO
    public function systemDashboard()
    {
        $opcache = null;
        $usuario = Auth::user();
        $habilitarDashboardAvancado = ($usuario->name === 'admin');

        if ($habilitarDashboardAvancado === true && !empty(env('FASTCGI_HOST'))) {
            $adapter = new FastCGI(env('FASTCGI_HOST'));
            $opcache = new CacheTool();
            $opcache->setAdapter($adapter);
            $opcache->addProxy(new OpcacheProxy());
        }

        $formatoData = 'd/m/Y - H:i:s - e (O T)';

        $timezoneLaravel = Config::get('app.timezone');
        $dateDefaultTimezone = date_default_timezone_get();
        $dataPhp = date($formatoData);
        $dataCarbon = Carbon::now()->tz('UTC')->format($formatoData);

        $isTimezoneUtc = ($timezoneLaravel === 'UTC' && $dateDefaultTimezone === 'UTC');
        $isDatasIguais = ($dataPhp === $dataCarbon);

        if ($isTimezoneUtc !== true) {
            app()->abort(500, 'Timezone incorreta: ' . $timezoneLaravel . ' --- ' . $dateDefaultTimezone);
        }

        if ($isDatasIguais !== true) {
            app()->abort(500, 'Erro ao comparar datas: ' . $dataPhp . ' ---' . $dataCarbon);
        }

        return view('admin.home.dashboard', [
            'habilitarDashboardAvancado' => $habilitarDashboardAvancado,
            'opcache' => $opcache,
            'isTimezoneUtc' => $isTimezoneUtc,
            'isDatasIguais' => $isDatasIguais,
            'timezoneLaravel' => $timezoneLaravel,
            'dateDefaultTimezone' => $dateDefaultTimezone,
            'dataPhp' => $dataPhp,
            'dataCarbon' => $dataCarbon,
        ]);
    }

    // ...
    // ...
    // ...
}
