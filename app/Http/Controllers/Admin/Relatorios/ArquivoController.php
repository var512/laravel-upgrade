<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin\Relatorios;

use App\Http\Controllers\Controller;
use App\Models\Admin\Relatorios\Arquivo;

class ArquivoController extends Controller
{
    public function listar()
    {
        return view('admin.relatorios.arquivo.listar');
    }

    public function ajaxEditar($arquivoId)
    {
        $item = Arquivo::findOrFail($arquivoId);

        return view('admin.relatorios.arquivo.partials.ajax_editar', [
            'item' => $item,
        ]);
    }
}
