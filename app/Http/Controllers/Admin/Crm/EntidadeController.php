<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin\Crm;

use App\Http\Controllers\Controller;
use App\Models\Admin\Crm\Entidade;
use Illuminate\Http\Request;

class EntidadeController extends Controller
{
    public function definir(Request $request, Entidade $entidade)
    {
        $request->session()->put('admin.crm.entidade.id', $entidade->id);

        return redirect()->route('admin.crm.dashboard.atendimento')->with('_mensagem', [
            'tipo' => 'sucesso',
            'texto' => 'Entidade escolhida: ' . $entidade->nome,
        ]);
    }
}
