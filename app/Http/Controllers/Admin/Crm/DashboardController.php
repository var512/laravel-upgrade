<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin\Crm;

use App\Http\Controllers\Controller;
use App\Models\Admin\Crm\Dashboard;

class DashboardController extends Controller
{
    public function atendimento()
    {
        $dashboard = new Dashboard();

        // ações pendentes e em atraso
        $acoesPendentes = $dashboard->acoesPendentes();
        $atendimentosRealizados = $dashboard->atendimentosRealizados();

        return view('admin.crm.dashboard.atendimento', [
            'acoesPendentes' => $acoesPendentes,
            'atendimentosRealizados' => $atendimentosRealizados,
        ]);
    }

    public function geral()
    {
        $dashboard = new Dashboard();

        $estatisticas = $dashboard->estatisticas();
        $totalEmpresasByStakeholder = $dashboard->totalEmpresasByStakeholder();
        $totalEmpresasByAtributo = $dashboard->totalEmpresasByAtributo();

        return view('admin.crm.dashboard.geral', [
            'estatisticas' => $estatisticas,
            'totalEmpresasByStakeholder' => $totalEmpresasByStakeholder,
            'totalEmpresasByAtributo' => $totalEmpresasByAtributo,
        ]);
    }
}
