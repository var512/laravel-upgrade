<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin\Crm;

use App\Http\Controllers\Controller;
use App\Models\Admin\Crm\Empresa;
use App\Models\Admin\Crm\GrupoEmpresarial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmpresaController extends Controller
{
    public function listar()
    {
        return view('admin.crm.empresa.listar');
    }

    public function visaoGeral($id)
    {
        $item = Empresa::with([
            'atributos',
            'contatos',
            'acoes' => function($query) {
                $query->whereNotNull('data_conclusao')->orderBy('data_conclusao', 'desc');
            },
        ])
            ->findOrFail($id);

        $empresasMesmoGrupoEmpresarial = Empresa::getEmpresasDoMesmoGrupoEmpresarial($item->grupo_empresarial_id);

        return view('admin.crm.empresa.visao_geral', [
            'item' => $item,
            'empresasMesmoGrupoEmpresarial' => $empresasMesmoGrupoEmpresarial,
        ]);
    }

    public function exportar($id)
    {
        $item = Empresa::with([
            'atributos',
            'contatos',
            'acoes' => function($query) {
                $query->whereNotNull('data_conclusao')->orderBy('data_conclusao', 'desc');
            },
        ])
            ->findOrFail($id);

        return view('admin.crm.empresa.exportar', [
            'item' => $item,
        ]);
    }

    public function edit($id)
    {
        // ...
        // ...
        // ...
    }

    public function editarAcoes($id)
    {
        $item = Empresa::findOrFail($id);
        $empresasMesmoGrupoEmpresarial = Empresa::getEmpresasDoMesmoGrupoEmpresarial($item->grupo_empresarial_id);

        return view('admin.crm.empresa.editar_acoes', [
            'item' => $item,
            'empresasMesmoGrupoEmpresarial' => $empresasMesmoGrupoEmpresarial,
        ]);
    }

    public function editarAnotacoes($id)
    {
        $item = Empresa::findOrFail($id);
        $empresasMesmoGrupoEmpresarial = Empresa::getEmpresasDoMesmoGrupoEmpresarial($item->grupo_empresarial_id);

        return view('admin.crm.empresa.editar_anotacoes', [
            'item' => $item,
            'empresasMesmoGrupoEmpresarial' => $empresasMesmoGrupoEmpresarial,
        ]);
    }

    public function editarContatos($id)
    {
        // ...
        // ...
        // ...
    }

    public function ajaxAssociarContatos($id)
    {
        $empresa = Empresa::findOrFail($id);

        return view('admin.crm.empresa.partials.ajax_associar_contatos', [
            'empresa' => $empresa,
        ]);
    }

    public function ajaxCriar(Request $request)
    {
        return view('admin.crm.empresa.partials.ajax_criar');
    }
}
