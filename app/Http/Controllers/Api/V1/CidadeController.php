<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\CidadeCollection;
use App\Models\Cidade;
use Illuminate\Http\Request;

class CidadeController extends AbstractApiController
{
    public function buscar(Request $request)
    {
        $q = (string) $request->input('q');

        $q = '%' . $q . '%';

        return new CidadeCollection(
            Cidade::take(200)
                ->where('nome', 'LIKE', $q)
                ->orderBy('nome', 'asc')
                ->get()
        );
    }
}
