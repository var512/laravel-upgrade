<?php

namespace App\Http\Controllers\Api\V1\Admin\Crm;

use App\Http\Controllers\Api\V1\AbstractApiController;
use App\Http\Resources\Admin\Crm\EmpresaCollection;
use App\Models\Admin\Crm\Empresa;
use Illuminate\Http\Request;

class EmpresaDataTableController extends AbstractApiController
{
    public function index(Request $request)
    {
        $data = new EmpresaCollection(
            Empresa::with([
                'atributos',
                'grupoEmpresarial',
                'cidade',
            ])
                ->dataTableFilter()
                ->paginate($this->resultsPerPage)
        );

        return $this->respond($data);
    }
}
