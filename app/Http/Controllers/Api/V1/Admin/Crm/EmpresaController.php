<?php

namespace App\Http\Controllers\Api\V1\Admin\Crm;

use App\Exceptions\ApiSemUsuarioIdException;
use App\Http\Controllers\Api\V1\AbstractApiController;
use App\Http\Requests\Admin\Crm\EmpresaRequest;
use App\Http\Requests\Admin\Crm\EmpresaUpdateContatosRequest;
use App\Http\Resources\Admin\Crm\EmpresaCollection;
use App\Models\Admin\Crm\Empresa;
use App\Models\Admin\Crm\Entidade;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmpresaController extends AbstractApiController
{
    public function store(EmpresaRequest $request)
    {
        $input = $request->all();

        $empresa = new Empresa();
        $empresa->fill($input);
        $empresa->save();

        return response([
            'id' => $empresa->id,
            'razao_social' => $empresa->razao_social,
        ], 200);
    }

    // ...
    // ...
    // ...

    public function buscar(Request $request)
    {
        $q = (string) $request->input('q');

        if (mb_strlen($q) < 1) {
            return [];
        }

        $cnpjQ = '%' . preg_replace('/[[:punct:]]/', '', $q) . '%';
        $q = '%' . $q . '%';

        return new EmpresaCollection(
            Empresa::take(200)
                ->where('razao_social', 'LIKE', $q)
                ->orWhere('nome_fantasia', 'LIKE', $q)
                ->orWhere('cnpj', 'LIKE', $cnpjQ)
                ->orderBy('razao_social', 'asc')
                ->get()
        );
    }
}
