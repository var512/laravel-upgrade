<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Relatorios;

use App\Http\Controllers\Api\V1\AbstractApiController;
use App\Http\Resources\Admin\Relatorios\ArquivoCollection;
use App\Models\Admin\Relatorios\Arquivo;
use Illuminate\Http\Request;

class ArquivoDataTableController extends AbstractApiController
{
    public function index(Request $request)
    {
        $data = new ArquivoCollection(
            Arquivo::dataTableFilter()
                ->paginate($this->resultsPerPage)
        );

        return $this->respond($data);
    }
}
