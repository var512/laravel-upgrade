<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Relatorios;

use App\Helpers\RelatoriosHelper;
use App\Http\Controllers\Api\V1\AbstractApiController;
use App\Http\Requests\Admin\Relatorios\ArquivoRequest;
use App\Http\Requests\Admin\Relatorios\ArquivoStoreRequest;
use App\Http\Requests\Request;
use App\Models\Admin\Relatorios\Arquivo;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

// TODO extrair response()s
class ArquivoController extends AbstractApiController
{
    public function download($arquivoId)
    {
        $arquivo = Arquivo::findOrFail($arquivoId);

        $storageFilepath = $this->getStorageFilepath($arquivo);
        $downloadFilename = $this->getDownloadFilename($arquivo);

        if (!Storage::disk('local')->has($storageFilepath)) {
            app()->abort('500', 'Não foi possível carregar o arquivo: ' . $storageFilepath);
        }

        $mimeType = Storage::disk('local')->mimeType($storageFilepath);
        $file = Storage::disk('local')->get($storageFilepath);

        return response()->make($file, 200, [
            'Content-Type' => $mimeType,
            'Content-Disposition' => 'inline;filename=' . $downloadFilename,
        ]);
    }

    public function store(ArquivoStoreRequest $request)
    {
        $file = $request->file('file');
        $nome = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $nome = mb_substr($nome, 0, 200);
        $extensao = $file->getClientOriginalExtension();
        $tamanho_bytes = $file->getSize();

        $arquivo = new Arquivo();
        $arquivo->nome = $nome;
        $arquivo->extensao = $extensao;
        $arquivo->tamanho_bytes = $tamanho_bytes;

        $arquivo->save();

        try {
            Storage::disk('local')
                ->put(
                    env('ARQUIVOS_RELATORIOS_PATH') . $arquivo->id . '.' . $extensao,
                    file_get_contents($file->getRealPath())
                );
        } catch (Exception $e) {
            $arquivo->delete();

            return response([
                $e->getMessage(),
            ], 400);
        }

        return response([
            'id' => $arquivo->id,
            'nome' => $arquivo->nome,
            'extensao' => $arquivo->extensao,
        ], 200);
    }

    public function update(ArquivoRequest $request, $id)
    {
        $arquivo = Arquivo::findOrFail($id);
        $input = $request->all();

        $arquivo->update($input);
    }

    public function destroy($id)
    {
        $arquivo = Arquivo::findOrFail($id);
        $storageFilepath = $this->getStorageFilepath($arquivo);

        try {
            Storage::disk('local')->delete($storageFilepath);

            $arquivo->delete();
        } catch (Exception $e) {
            return response([
                $e->getMessage(),
            ], 400);
        }

        return response([
            'id' => $arquivo->id,
            'nome' => $arquivo->nome,
            'extensao' => $arquivo->extensao,
        ], 200);
    }

    /**
     * Nome do arquivo que o usuário vai fazer o download
     */
    protected function getDownloadFilename(Arquivo $arquivo): string
    {
        $filename = '';

        $filename .= $arquivo->id;
        $filename .= '-' . Str::slug($arquivo->nome);
        $filename .= '.' . $arquivo->extensao;

        return $filename;
    }

    /**
     * Path do arquivo salvo no storage
     */
    protected function getStorageFilepath(Arquivo $arquivo): string
    {
        $path = '';

        $path .= env('ARQUIVOS_RELATORIOS_PATH');
        $path .= $arquivo->id;
        $path .= '.' . $arquivo->extensao;

        return $path;
    }
}
