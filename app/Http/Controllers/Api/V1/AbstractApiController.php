<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\V1;

use App\Exceptions\EntidadeIdNotSetException;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

abstract class AbstractApiController extends Controller
{
    private Request $request;
    protected int $resultsPerPage;
    protected int $entidadeId;

    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->resultsPerPage = (int) $request->input('resultsPerPage') ?? 15;
        $this->entidadeId = (int) $request->input('entidadeId');

        if ($this->entidadeId === 0) {
            throw new EntidadeIdNotSetException('Informe o ID da entidade desejada');
        }

        session(['admin.crm.entidade.id' => $this->entidadeId]);
    }

    /**
     * Build the response
     */
    protected function respond($content = [], int $statusCode = 200, array $headers = [])
    {
        $data = $this->mapContentToResponseData($content);

        return new JsonResponse($data, $statusCode, $headers);
    }

    /**
     * Map the response
     */
    protected function mapContentToResponseData($content)
    {
        if (empty($content->resource)) {
            throw new ResourceNotFoundException();
        }

        $paginated = $content->resource;

        $links = [
            'first' => $paginated->url(1),
            'last' => $paginated->url($paginated->lastPage()),
            'prev' => $paginated->previousPageUrl(),
            'next' => $paginated->nextPageUrl(),
        ];

        $meta = [
            'current_page' => $paginated->currentPage(),
            'from' => $paginated->firstItem(),
            'last_page' => $paginated->lastPage(),
            'path' => $paginated->path(),
            'per_page' => $paginated->perPage(),
            'to' => $paginated->lastItem(),
            'total' => $paginated->total(),
        ];

        return [
            'data' => $paginated->getCollection(),
            'links' => $links,
            'meta' => $meta,

            // DataTable
            // TODO ver implementação fractal antiga e repensar
            'draw' => (int) $this->request->input('draw'),
            'recordsTotal' => $paginated->total(),
            'recordsFiltered' => $paginated->total(),
        ];
    }
}
