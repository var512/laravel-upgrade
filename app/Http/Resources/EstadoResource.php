<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EstadoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     */
    public function toArray($request): array
    {
        return [
            'id' => (int) $this->id,
            'nome' => $this->nome,
            'sigla' => $this->sigla,
            'cidade' => [ 'data' => CidadeResource::make($this->whenLoaded('cidade')) ],
        ];
    }
}
