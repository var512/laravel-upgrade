<?php

declare(strict_types=1);

namespace App\Http\Resources\Admin\Crm;

use App\Http\Resources\CidadeResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EmpresaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     */
    public function toArray($request): array
    {
        $data = [
            'id' => (int) $this->id,
            'razao_social' => (string) $this->razao_social,
            'nome_fantasia' => (string) $this->nome_fantasia,

            // ...
            // ...
            // ...

            'cidade' => [ 'data' => CidadeResource::make($this->whenLoaded('cidade')) ],
            'grupoEmpresarial' => [ 'data' => GrupoEmpresarialResource::make($this->whenLoaded('grupoEmpresarial')) ],
            'atributos' => [ 'data' => AtributoResource::collection($this->whenLoaded('atributos')) ],
        ];

        // quando aparece na lista de empresas do contato
        if (isset($this->pivot->rel_empresa_is_contato_principal)) {
            $data['rel_empresa_is_contato_principal'] = (bool) $this->pivot->rel_empresa_is_contato_principal;
        }

        return $data;
    }
}
