<?php

declare(strict_types=1);

namespace App\Http\Resources\Admin\Relatorios;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ArquivoCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     */
    public $collects = ArquivoResource::class;

    /**
     * Transform the resource collection into an array.
     */
    public function toArray($request): array
    {
        return [
            'data' => $this->collection,
        ];
    }
}
