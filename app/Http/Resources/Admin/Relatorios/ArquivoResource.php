<?php

declare(strict_types=1);

namespace App\Http\Resources\Admin\Relatorios;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArquivoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     */
    public function toArray($request): array
    {
        $createdAtNaTimezoneAtiva = !empty($this->createdAtNaTimezoneAtiva)
            ? $this->createdAtNaTimezoneAtiva->format('d/m/Y H:i')
            : '';

        return [
            'id' => (int) $this->id,
            'nome' => $this->nome,
            'extensao' => $this->extensao,
            'tamanho_bytes' => (int) $this->tamanho_bytes,
            'tamanhoHuman' => $this->tamanhoHuman,
            // ...
            // ...
            // ...
            'createdAtNaTimezoneAtiva' => $createdAtNaTimezoneAtiva,
        ];
    }
}
