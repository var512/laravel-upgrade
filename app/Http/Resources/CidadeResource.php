<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Http\Resources\Admin\Crm\EmpresaResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CidadeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     */
    public function toArray($request): array
    {
        return [
            'id' => (int) $this->id,
            'nome' => $this->nome,
            'nomeComUf' => $this->nomeComUf,
            'empresa' => [ 'data' => EmpresaResource::make($this->whenLoaded('empresa')) ],
            'estado' => [ 'data' => EstadoResource::make($this->whenLoaded('estado')) ],
        ];
    }
}
