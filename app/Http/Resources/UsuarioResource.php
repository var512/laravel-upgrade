<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UsuarioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     */
    public function toArray($request): array
    {
        $createdAtNaTimezoneAtiva = !empty($this->createdAtNaTimezoneAtiva)
            ? $this->createdAtNaTimezoneAtiva->format('d/m/Y H:i')
            : '';

        return [
            'id' => (int) $this->id,
            'email' => $this->email,
            'nome' => $this->nome,
            'createdAtNaTimezoneAtiva' => $createdAtNaTimezoneAtiva,
        ];
    }
}
