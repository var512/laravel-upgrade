<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Igpm extends Model
{
    protected $table = 'lista_igpm';
}
