<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Admin\Crm\Empresa;
use App\Models\Traits\TimezoneAwareModelTrait;
use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    use TimezoneAwareModelTrait;

    protected $table = 'lista_cidades';

    /****************************************************
     * relacionamentos
     ****************************************************/
    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id');
    }

    public function empresas()
    {
        return $this->hasMany(Empresa::class);
    }

    /****************************************************
     * accessors
    ****************************************************/
    public function getNomeComUfAttribute($value)
    {
        $str = '';

        $str .= $this->nome;
        $str .= ' (';
        $str .= $this->estado->sigla;
        $str .= ')';

        return $str;
    }
}
