<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Traits\TimezoneAwareModelTrait;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    use TimezoneAwareModelTrait;

    protected $table = 'lista_estados';

    /****************************************************
     * relacionamentos
     ****************************************************/
    public function cidades()
    {
        return $this->hasMany(Cidade::class);
    }
}
