<?php

declare(strict_types=1);

namespace App\Models\Admin\Relatorios;

use App\Domain\DataTable\DataTable;
use App\Helpers\UnitHelper;
use App\Models\Traits\TimezoneAwareModelTrait;
use Illuminate\Database\Eloquent\Model;

class Arquivo extends Model
{
    use TimezoneAwareModelTrait;

    protected $table = 'rlt_arquivos';
    protected $guarded = ['id', 'nome', 'extensao'];

    /****************************************************
     * acessors
     ****************************************************/
    public function getTamanhoHumanAttribute()
    {
        return UnitHelper::formatarBytes($this->tamanho_bytes);
    }

    /****************************************************
     * datatables
     ****************************************************/
    public function scopeDatatableFilter($query)
    {
        $datatable = new DataTable();

        // ...
        // ...
        // ...

        return $query;
    }
}
