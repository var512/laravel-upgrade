<?php

declare(strict_types=1);

namespace App\Models\Admin\Crm;

use App\Models\Traits\TimezoneAwareModelTrait;
use Illuminate\Database\Eloquent\Model;

class Entidade extends Model
{
    use TimezoneAwareModelTrait;

    protected $table = 'crm_entidades';

    protected $fillable = [
        'nome',
    ];

    /****************************************************
     * relacionamentos
     ****************************************************/
    public function acoes()
    {
        return $this->hasMany(Acao::class);
    }

    public function anotacoes()
    {
        return $this->hasMany(Anotacao::class);
    }

    public function contatos()
    {
        return $this->hasMany(Contato::class);
    }

    public function empresas()
    {
        return $this->hasMany(Empresa::class);
    }

    public function gruposempresariais()
    {
        return $this->hasMany(GrupoEmpresarial::class);
    }

    public function atributos()
    {
        return $this->hasMany(Atributo::class);
    }
}
