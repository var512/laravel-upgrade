<?php

declare(strict_types=1);

namespace App\Models\Admin\Crm;

use App\Domain\DataTable\DataTable;
use App\Helpers\Util;
use App\Models\Cidade;
use App\Models\Scopes\EntidadeFromSessionScope;
use App\Models\Traits\TimezoneAwareModelTrait;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use TimezoneAwareModelTrait;

    protected $table = 'crm_empresas';

    protected $fillable = [
        'razao_social',
        'nome_fantasia',
        // ...
        // ...
        // ...
    ];

    protected static function boot(): void
    {
        parent::boot();
        static::addGlobalScope(new EntidadeFromSessionScope());
    }

    /****************************************************
     * relacionamentos
    ****************************************************/
    public function entidade()
    {
        return $this->belongsTo(Entidade::class, 'entidade_id');
    }

    public function cidade()
    {
        return $this->belongsTo(Cidade::class, 'cidade_id');
    }

    public function grupoEmpresarial()
    {
        return $this->belongsTo(GrupoEmpresarial::class, 'grupo_empresarial_id');
    }

    public function contatos()
    {
        return $this->belongsToMany(Contato::class, 'crm_rel_empresa_contato')
            ->orderBy('rel_empresa_is_contato_principal', 'desc')
            ->orderBy('nome', 'asc')
            ->withPivot('rel_empresa_is_contato_principal');
    }

    public function atributos()
    {
        return $this->belongsToMany(Atributo::class, 'crm_rel_empresa_atributo');
    }

    public function acoes()
    {
        return $this->hasMany(Acao::class);
    }

    public function anotacoes()
    {
        return $this->hasMany(Anotacao::class);
    }

    /****************************************************
     * acessors
     ****************************************************/
    public function getCnpjAttribute($value)
    {
        return Util::formatarCnpj($value);
    }

    public function getEnderecoCompletoAttribute()
    {
        $endereco = [
            $this->endereco,
            $this->bairro,
            $this->cep,
        ];

        if (!empty($this->cidade)) {
            $endereco[] = $this->cidade->nome;
            $endereco[] = $this->cidade->estado->sigla;
        }

        $endereco = array_filter($endereco);

        return implode(' - ', $endereco);
    }

    // ...
    // ...
    // ...

    /****************************************************
     * mutators
     ****************************************************/
    /**
     * Salva CNPJ apenas com números
     */
    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj'] = preg_replace('/\D/', '', $value);
    }

    /****************************************************
     * datatables
     ****************************************************/
    public function scopeDatatableFilter($query)
    {
        $datatable = new DataTable();

        // ...
        // ...
        // ...

        return $query;
    }
}
