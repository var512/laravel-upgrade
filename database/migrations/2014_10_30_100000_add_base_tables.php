<?php

declare(strict_types=1);

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = Carbon::now();

        // TODO re-adicionar indexes

        /****************************************************
         * usuários
         ****************************************************/
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');

            // ...
            // ...
            // ...

            $table->rememberToken();
            $table->timestamps();
        });

        /****************************************************
         * crm
         ****************************************************/
        Schema::create('crm_entidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 60);

            // ...
            // ...
            // ...

            $table->timestamps();
        });

        Schema::create('crm_grupos_empresariais', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('entidade_id')->unsigned();
            $table->string('nome', 60);

            $table->timestamps();
            $table->foreign('entidade_id')->references('id')->on('crm_entidades');
        });

        Schema::create('crm_contatos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('entidade_id')->unsigned();
            $table->string('nome', 200);

            // ...
            // ...
            // ...

            $table->timestamps();
            $table->foreign('entidade_id')->references('id')->on('crm_entidades');
        });

        Schema::create('crm_empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('entidade_id')->unsigned();
            $table->string('razao_social', 200);
            $table->string('nome_fantasia', 200)->nullable();
            $table->string('cnpj', 24)->nullable();

            // ...
            // ...
            // ...

            $table->bigInteger('cidade_id')->unsigned()->nullable();
            $table->bigInteger('grupo_empresarial_id')->nullable()->unsigned();

            $table->timestamps();
            $table->foreign('entidade_id')->references('id')->on('crm_entidades');
            $table->foreign('cidade_id')->references('id')->on('lista_cidades');
            $table->foreign('grupo_empresarial_id')->references('id')->on('crm_grupos_empresariais');
        });

        Schema::create('crm_rel_empresa_contato', function (Blueprint $table) {
            $table->bigInteger('empresa_id')->unsigned();
            $table->bigInteger('contato_id')->unsigned();
            $table->bigInteger('rel_empresa_is_contato_principal')->default(0)->unsigned();

            $table->foreign('empresa_id')->references('id')->on('crm_empresas');
            $table->foreign('contato_id')->references('id')->on('crm_contatos');

            $table->primary(['empresa_id', 'contato_id']);
        });

        Schema::create('crm_anotacoes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('entidade_id')->unsigned();
            $table->dateTime('data');

            // ...
            // ...
            // ...

            $table->bigInteger('empresa_id')->unsigned();
            $table->bigInteger('usuario_id')->unsigned();

            $table->foreign('entidade_id')->references('id')->on('crm_entidades');
            $table->foreign('empresa_id')->references('id')->on('crm_empresas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');

            $table->timestamps();
        });

        Schema::create('crm_acoes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('entidade_id')->unsigned();
            $table->dateTime('data_promessa');
            $table->dateTime('data_conclusao')->nullable();

            // ...
            // ...
            // ...

            $table->bigInteger('empresa_id')->unsigned();
            $table->bigInteger('usuario_id')->unsigned();

            $table->foreign('entidade_id')->references('id')->on('crm_entidades');
            $table->foreign('empresa_id')->references('id')->on('crm_empresas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');

            $table->timestamps();
        });

        Schema::create('crm_rel_acao_contato', function (Blueprint $table) {
            $table->bigInteger('acao_id')->unsigned();
            $table->bigInteger('contato_id')->unsigned();

            $table->foreign('acao_id')->references('id')->on('crm_acoes');
            $table->foreign('contato_id')->references('id')->on('crm_contatos');

            $table->primary(['acao_id', 'contato_id']);
        });

        Schema::create('crm_atributos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('entidade_id')->unsigned();
            $table->string('nome', 120);

            $table->timestamps();
            $table->foreign('entidade_id')->references('id')->on('crm_entidades');
        });

        Schema::create('crm_rel_empresa_atributo', function (Blueprint $table) {
            $table->bigInteger('empresa_id')->unsigned();
            $table->bigInteger('atributo_id')->unsigned();

            $table->foreign('empresa_id')->references('id')->on('crm_empresas');
            $table->foreign('atributo_id')->references('id')->on('crm_atributos');
            $table->primary(['empresa_id', 'atributo_id']);
        });

        /****************************************************
         * relatórios
        ****************************************************/
        Schema::create('rlt_arquivos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 250);
            $table->string('extensao', 16);
            $table->integer('tamanho_bytes')->unsigned();
            $table->text('texto')->nullable();

            // ...
            // ...
            // ...

            $table->timestamps();
        });

        Schema::create('rlt_gdp_familias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 250)->nullable();
            $table->string('extensao', 16);
            $table->text('texto')->nullable();

            // ...
            // ...
            // ...

            $table->timestamps();
        });

        Schema::create('rlt_gdp_produtos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 250)->nullable();
            $table->string('descricao', 500)->nullable();
            $table->bigInteger('familia_id')->unsigned()->nullable();

            // ...
            // ...
            // ...

            $table->foreign('familia_id')->references('id')->on('rlt_gdp_familias');

            $table->timestamps();
        });

        Schema::create('rlt_gdp_itens', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('documento', 120)->nullable();

            // ...
            // ...
            // ...

            $table->bigInteger('produto_id')->unsigned()->nullable();

            $table->foreign('produto_id')->references('id')->on('rlt_gdp_produtos');

            $table->timestamps();
        });

        /****************************************************
         * dados genéricos
        ****************************************************/
        // ...
        // ...
        // ...
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
