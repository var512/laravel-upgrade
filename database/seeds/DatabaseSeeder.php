<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        if (app()->environment() !== 'local') {
            app()->abort(500, 'Seeding disabled');
        }

        $timeStart = microtime(true);

        DB::disableQueryLog();

        // TODO optimize
        $this->call([
            CrmSeeder::class,
        ]);

        $timeEnd = microtime(true);
        $executionTime = ($timeEnd - $timeStart) / 60;
        $this->command->info('Seeded (' . round($executionTime, 2) . ' minutes)');
    }
}
