<?php

declare(strict_types=1);

use App\Models\Admin\Crm\Acao;
use App\Models\Admin\Crm\Anotacao;
use App\Models\Admin\Crm\Atributo;
use App\Models\Admin\Crm\Contato;
use App\Models\Admin\Crm\Empresa;
use App\Models\Admin\Crm\Entidade;
use App\Models\Admin\Crm\GrupoEmpresarial;
use App\Models\Admin\Crm\Produto;
use App\Models\Usuario;
use Illuminate\Database\Seeder;

class CrmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->command->comment('Seeding: App\Models\Usuario');
        factory(Usuario::class, 30)->create();

        $this->command->comment('Seeding: App\Models\Admin\Crm\Entidade');
        $entidades = factory(Entidade::class, 3)->create();

        foreach ($entidades as $entidade) {
            // usa o global scope EntidadeFromSession para relacionamentos serem da mesma entidade
            session(['admin.crm.entidade.id' => $entidade->id]);

            $this->command->comment('-- Entidade #' . $entidade->id . ' (' . $entidade->nome . ')');

            $this->command->comment('Seeding: App\Models\Admin\Crm\Atributo');
            factory(Atributo::class, 15)->create([
                'entidade_id' => $entidade->id,
            ]);

            $this->command->comment('Seeding: App\Models\Admin\Crm\Contato');
            factory(Contato::class, 150)->create([
                'entidade_id' => $entidade->id,
            ]);

            $this->command->comment('Seeding: App\Models\Admin\Crm\GrupoEmpresarial');
            factory(GrupoEmpresarial::class, 12)->create([
                'entidade_id' => $entidade->id,
            ]);

            $this->command->comment('Seeding: App\Models\Admin\Crm\Empresa');
            factory(Empresa::class, 500)->create([
                'entidade_id' => $entidade->id,
            ]);

            $this->command->comment('Seeding: App\Models\Admin\Crm\Acao');
            factory(Acao::class, 100)->create([
                'entidade_id' => $entidade->id,
            ]);
            factory(Acao::class, 50)->states('concluida')->create([
                'entidade_id' => $entidade->id,
            ]);

            $this->command->comment('Seeding: App\Models\Admin\Crm\Anotacao');
            factory(Anotacao::class, 50)->create([
                'entidade_id' => $entidade->id,
            ]);

            $this->command->comment('Modifying pivot tables');
            DB::update('
                UPDATE crm_rel_empresa_contato
                SET rel_empresa_is_contato_principal = ROUND(RAND())
            ');
        }
    }
}
