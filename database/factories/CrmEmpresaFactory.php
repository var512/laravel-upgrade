<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Helpers\ArrayHelper;
use App\Models\Admin\Crm\Atributo;
use App\Models\Admin\Crm\Contato;
use App\Models\Admin\Crm\Empresa;
use App\Models\Admin\Crm\GrupoEmpresarial;
use App\Models\Admin\Crm\Produto;
use App\Models\Cidade;
use Faker\Generator as Faker;

$factory->define(Empresa::class, function (Faker $faker) {
    return [
        'razao_social' => $faker->unique()->company,
        'nome_fantasia' => $faker->unique()->company,
        'cnpj' => $faker->unique()->cnpj,
        // ...
        // ...
        // ...
        'created_at' => $faker->dateTimeBetween('-10 years', '-5 years'),
        'updated_at' => $faker->dateTimeBetween('-4 years'),
    ];
});

$factory->afterMaking(Empresa::class, function ($empresa, $faker) {
    // ...
    // ...
    // ...

    $empresa->principais_produtos = $principaisProdutos;
    $empresa->principais_clientes = $principaisClientes;
    $empresa->principais_fornecedores = $principaisFornecedores;
});

$factory->afterCreating(Empresa::class, function ($empresa) {
    // Cidade
    if (rand(0, 2) !== 0) {
        $cidade = Cidade::whereIn('estado_id', [23, 24])->inRandomOrder()->first();
        $empresa->cidade()->associate($cidade)->save();
    }

    // ...
    // ...
    // ...

    // Atributo
    if (rand(0, 12) !== 0) {
        $atributo = Atributo::inRandomOrder()->limit(rand(1, 4))->pluck('id')->toArray();
        $empresa->atributos()->sync($atributo);
    }
});
