<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Helpers\DateHelper;
use App\Models\Admin\Crm\Acao;
use App\Models\Admin\Crm\Contato;
use App\Models\Admin\Crm\Empresa;
use App\Models\Usuario;
use Faker\Generator as Faker;

$factory->define(Acao::class, function (Faker $faker) {
    return [
        'data_promessa' => DateHelper::dateTimeAleatorio(180, 100),
        'categoria' => $faker->randomElement(array_keys(Acao::listaCategoria())),
        'detalhes' => $faker->realText(mt_rand(10, 2000), 2),
        // ...
        // ...
        // ...
        'created_at' => $faker->dateTimeBetween('-10 years', '-5 years'),
        'updated_at' => $faker->dateTimeBetween('-4 years'),
    ];
});

$factory->state(Acao::class, 'concluida', function () {
    return [
        'data_conclusao' => DateHelper::dateTimeAleatorio(90, 60),
    ];
});

$factory->afterMaking(Acao::class, function ($acao) {
    $empresa = Empresa::inRandomOrder()->first();
    $acao->empresa()->associate($empresa);

    $usuario = Usuario::inRandomOrder()->first();
    $acao->usuario()->associate($usuario);

    $acao->save();
});

$factory->afterCreating(Acao::class, function ($acao) {
    if (rand(0, 6) !== 0) {
        $contato = Contato::inRandomOrder()->limit(rand(1, 9))->pluck('id')->toArray();
        $acao->contatos()->sync($contato);
    }
});
