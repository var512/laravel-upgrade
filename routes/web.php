<?php

declare(strict_types=1);

/****************************************************
 * Admin
 ****************************************************/
$adminRouteGroup = [
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => [
        // ...
        // ...
        // ...
    ],
];

Route::group($adminRouteGroup, function () {
    /****************************************************
     * App\Http\Controllers\Admin
     ****************************************************/
    Route::get('/', [
        'as' => 'admin.home',
        'uses' => 'HomeController@home',
    ]);

    // ...
    // ...
    // ...

    /****************************************************
     * App\Http\Controllers\Admin\ReceitaWs
     ****************************************************/
    $receitawsRouteGroup = [
        'as' => 'admin.receitaws.',
        'prefix' => 'receitaws',
        'namespace' => 'ReceitaWs',
    ];

    Route::group($receitawsRouteGroup, function () {
        /****************************************************
         * mass import
        ****************************************************/
        Route::get('importarEmpresas', [
            'as' => 'importarEmpresas',
            'uses' => 'ReceitaWsController@importarEmpresas',
        ]);
    });

    /****************************************************
     * App\Http\Controllers\Admin\Relatorios
     ****************************************************/
    $relatoriosRouteGroup = [
        'as' => 'admin.relatorios.',
        'prefix' => 'relatorios',
        'namespace' => 'Relatorios',
    ];

    Route::group($relatoriosRouteGroup, function () {
        /****************************************************
         * arquivo
         ****************************************************/
        // ...
        // ...
        // ...

        /****************************************************
         * importador
         ****************************************************/
        // ...
        // ...
        // ...

        /****************************************************
         * visualizador
         ****************************************************/
        // ...
        // ...
        // ...
    });

    /****************************************************
     * App\Http\Controllers\Admin\Crm
     ****************************************************/
    $crmRouteGroup = [
        'as' => 'admin.crm.',
        'prefix' => 'crm',
        'namespace' => 'Crm',
    ];

    Route::group($crmRouteGroup, function () {
        // ...
        // ...
        // ...
    });
});

/****************************************************
 * Clientes
 ****************************************************/
$clientesRouteGroup = [
    'prefix' => 'clientes',
    'namespace' => 'Clientes',
    'middleware' => [
        // ...
        // ...
        // ...
    ],
];

Route::group($clientesRouteGroup, function () {
    // ...
    // ...
    // ...
});
