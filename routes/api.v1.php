<?php

declare(strict_types=1);

/****************************************************
 * App\Http\Controllers\Api\V1
 ****************************************************/
$apiV1RouteGroup = [
    'namespace' => 'Api\V1',
];

/****************************************************
 * cidade
 ****************************************************/
Route::group($apiV1RouteGroup, function () {
    Route::get('cidade/buscar', [
        'as' => 'cidade.buscar',
        'uses' => 'CidadeController@buscar',
    ]);
});

// ...
// ...
// ...

/****************************************************
 * Admin
 ****************************************************/
$adminRouteGroup = [
    'prefix' => 'admin',
    'namespace' => 'Api\V1\Admin',
];

Route::group($adminRouteGroup, function () {
    // ...
    // ...
    // ...

    /****************************************************
     * App\Http\Controllers\Admin\Relatorios
     ****************************************************/
    $relatoriosRouteGroup = [
        'as' => 'admin.relatorios.',
        'prefix' => 'relatorios',
        'namespace' => 'Relatorios',
    ];

    Route::group($relatoriosRouteGroup, function () {
        /****************************************************
         * arquivo
         ****************************************************/
        // ...
        // ...
        // ...

        /****************************************************
         * importador
         ****************************************************/
        // ...
        // ...
        // ...

        /****************************************************
         * visualizador
         ****************************************************/
        // ...
        // ...
        // ...
    });

    /****************************************************
     * App\Http\Controllers\Api\V1\Admin\Crm
     ****************************************************/
    $crmRouteGroup = [
        'as' => 'admin.crm.',
        'prefix' => 'crm',
        'namespace' => 'Crm',
    ];

    Route::group($crmRouteGroup, function () {
        /****************************************************
         * ação
         ****************************************************/
        // ...
        // ...
        // ...

        /****************************************************
         * anotação
         ****************************************************/
        // ...
        // ...
        // ...

        /****************************************************
         * empresa
         ****************************************************/
        // ...
        // ...
        // ...

        Route::get('empresa/buscar', [
            'as' => 'empresa.buscar',
            'uses' => 'EmpresaController@buscar',
        ]);

        Route::put('empresa/{empresa}/update-contatos', [
            'as' => 'empresa.updateContatos',
            'uses' => 'EmpresaController@updateContatos',
        ]);

        Route::apiResource('empresaDataTable', 'EmpresaDataTableController', [
            'only' => [
                'index',
            ],
        ]);

        Route::apiResource('empresa', 'EmpresaController', [
            'only' => [
                'store',
                'update',
                'destroy',
            ],
        ]);

        /****************************************************
         * contato
         ****************************************************/
        Route::get('contato/buscar', [
            'as' => 'contato.buscar',
            'uses' => 'ContatoController@buscar',
        ]);

        Route::post('contato/setContatoPrincipal', [
            'as' => 'contato.setContatoPrincipal',
            'uses' => 'ContatoController@setContatoPrincipal',
        ]);

        Route::get('contato/empresa/{empresa}', [
            'as' => 'contatoDataTable.contatosByEmpresa',
            'uses' => 'ContatoDataTableController@contatosByEmpresa',
        ]);

        Route::apiResource('contatoDataTable', 'ContatoDataTableController', [
            'only' => [
                'index',
            ],
        ]);

        Route::apiResource('contato', 'ContatoController', [
            'only' => [
                'update',
                'store',
                'destroy',
            ]
        ]);

        /****************************************************
         * atributo
         ****************************************************/
        // ...
        // ...
        // ...

        /****************************************************
         * grupo empresarial
         ****************************************************/
        // ...
        // ...
        // ...
    });
});
