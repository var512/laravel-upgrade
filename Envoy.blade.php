{{-- TODO atualizar para nova estrutura e php 7.4, CI? --}}

@servers(['web' => '127.0.0.1'])

@setup
    date_default_timezone_set('UTC');
    mb_internal_encoding('UTF-8');

    $repo = 'git@gitlab.com:';
    $release = 'release_' . date('Y_m_d_H_i_s');

    $currentReleaseDir = '/var/www/app/current';
    $releasesDir = '/var/www/app/releases';
    $sharedDir = '/var/www/app/shared';
@endsetup

@macro('deploy')
    fetchRepo
    runComposer
    artisanDown
    updateSymlinks
    runMigrations
    cacheSetup
    artisanUp
    cleanOldReleases
@endmacro

@task('fetchRepo', ['on' => 'web'])
    echo '--- Task: fetchRepo';

    cd {{ $releasesDir }};
    git clone --depth 1 --branch master {{ $repo }} {{ $release }};
@endtask

@task('runComposer', ['on' => 'web'])
    echo '--- Task: runComposer';

    cd {{ $releasesDir }}/{{ $release }};
    composer install --prefer-dist --no-dev --no-scripts;
@endtask

@task('artisanDown', ['on' => 'web'])
    echo '--- Task: artisanDown';

    [ ! -d {{ $currentReleaseDir }} ] || cd {{ $currentReleaseDir }};
    [ ! -d {{ $currentReleaseDir }} ] || php artisan down;
@endtask

@task('artisanUp', ['on' => 'web'])
    echo '--- Task: artisanUp';

    [ ! -d {{ $currentReleaseDir }} ] || cd {{ $currentReleaseDir }};
    [ ! -d {{ $currentReleaseDir }} ] || php artisan up;
@endtask

@task('updateSymlinks', ['on' => 'web'])
    echo '--- Task: updateSymlinks';

    rm -rf {{ $currentReleaseDir }}
    ln -nfs {{ $releasesDir }}/{{ $release }} {{ $currentReleaseDir }};

    echo '--- Current release changed to: {{ $releasesDir }}/{{ $release }}';

    rm -rf {{ $currentReleaseDir }}/storage
    ln -nfs {{ $sharedDir }}/storage {{ $currentReleaseDir }}/storage;

    [ -f {{ $sharedDir }}/.env ] || touch {{ $sharedDir }}/.env;
    ln -nfs {{ $sharedDir }}/.env {{ $releasesDir }}/{{ $release }}/.env;

    echo 'Restarting php7.4-fpm';
    sudo systemctl restart php7.4-fpm.service

    echo 'Restarting nginx';
    sudo systemctl restart nginx.service;
@endtask

@task('cacheSetup', ['on' => 'web'])
    echo '--- Task: cacheSetup';

    cd {{ $releasesDir }}/{{ $release }};

    {{-- php artisan config:cache --}}

    php artisan config:clear
    php artisan route:cache
@endtask

@task('runMigrations', ['on' => 'web'])
    echo '--- Task: runMigrations';

    cd {{ $currentReleaseDir }};

    php artisan migrate --force;
@endtask

@task('cleanOldReleases', ['on' => 'web'])
    echo '--- Task: cleanOldReleases';

    cd {{ $releasesDir }};

    ls -1d release_* | head -n -5 | xargs -d "\n" rm -rf;
@endtask
